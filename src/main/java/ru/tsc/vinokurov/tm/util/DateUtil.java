package ru.tsc.vinokurov.tm.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    String PATTERN = "yyyy.MM.dd";
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (ParseException e) {
            System.err.println(e.getMessage());
            return null;
        }
    }

    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }
}
