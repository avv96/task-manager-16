package ru.tsc.vinokurov.tm.api.service;

import ru.tsc.vinokurov.tm.model.Project;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
