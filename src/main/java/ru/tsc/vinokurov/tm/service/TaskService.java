package ru.tsc.vinokurov.tm.service;

import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.service.ITaskService;
import ru.tsc.vinokurov.tm.enumerated.Sort;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.vinokurov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.vinokurov.tm.exception.field.*;
import ru.tsc.vinokurov.tm.model.Task;

import java.util.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return taskRepository.create(name, description);
    }

    @Override
    public Task create(String name, String description, Date dateBegin, Date dateEnd) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateBeginEmptyException();
        if (dateEnd == null) throw new DateEndEmptyException();
        Task task = taskRepository.create(name, description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        return task;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return taskRepository.findAll(sort.getComparator());
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
